# If a system zprofile exists, assume it did this for us
if [ \! -e /etc/zsh/zprofile -a \! -e /etc/zprofile ]; then
    emulate sh -c 'source /etc/profile'
fi
