# auto-rotate JPEGs based on EXIF info
function exifautotrans()
{
    if [ "x$1" = "x--backup-dir" -o "x$1" = "x-b" ]; then
        if [ "x$2" = "x" ]; then
            echo "$1 requires an argument" >&2
            return 1
        fi
        if [ ! -d "$2" ]; then
            echo "$2 is not a directory" >&2
            return 1
        fi
        backupdir="$2"
        shift; shift
    else
        backupdir="exifautotrans-backups"
        [ -d "$backupdir" ] || mkdir "$backupdir"
    fi
    for file in "$@"; do
        if [ ! -f "$file" ]; then
            echo "$file is not a file" >&2
        else
            case `jpegexiforient -n "$file"` in
                1) transform="";;
                2) transform="-flip horizontal";;
                3) transform="-rotate 180";;
                4) transform="-flip vertical";;
                5) transform="-transpose";;
                6) transform="-rotate 90";;
                7) transform="-transverse";;
                8) transform="-rotate 270";;
                *) transform="";;
            esac
            if test -n "$transform"; then
                echo "Executing: jpegtran -copy all $transform $file"
                jpegtran -copy all ${=transform} "$file" > exifautotrans_tempfile
                if test $? -ne 0; then
                    echo "Error while transforming $file - skipped."
                else
                    mv "$file" "$backupdir/$file" && \
                    mv exifautotrans_tempfile "$file" && \
                    jpegexiforient -1 "$file" > /dev/null
                fi
            fi
        fi
    done
}
