function playram {
    if [ -z "$1" ]; then
        return
    fi
    local dldir=$(mktemp -d)
    local ramfile="$dldir/$(basename "$1")"
    wget -P "$dldir" "$1"
    if [ ! -e "$ramfile" ]; then
        echo "Download failed"
        rmdir "$tmpdir"
    fi
    mplayer "$(cat "$ramfile")"
    rm "$ramfile"
    rmdir "$tmpdir"
}
