# shrink photos to max 600px
function shrink()
{
    if [ "x$1" = "x--dir" -o "x$1" = "x-d" ]; then
        if [ "x$2" = "x" ]; then
            echo "$1 requires an argument" >&2
            return 1
        fi
        if [ ! -d "$2" ]; then
            echo "$2 is not a directory" >&2
            return 1
        fi
        dest="$2"
        shift; shift
    else
        dest="shrunk"
        [ -d "$dest" ] || mkdir "$dest"
    fi
    longest_edge=600
    for file in "$@"; do
        if [ ! -f "$file" ]; then
            echo "$file is not a file" >&2
        else
            if [ $(identify -format '%h' "$file") -gt $(identify -format '%w' "$file") ]; then
                # Portrait
                echo "Shrinking $file [portrait]"
                convert "$file" -resize x600 -quality 85 "$dest/$file"
            else
                # Landscape (or square, I suppose)
                echo "Shrinking $file [landscape]"
                convert "$file" -resize 600 -quality 85 "$dest/$file"
            fi
        fi
    done
}
